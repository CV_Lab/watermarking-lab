# Watermarking Lab
# 创建人：曾逸夫
# 创建时间：2022-08-08
# 功能描述：批量图片加水印

import math
import os
import shutil
import sys
import zipfile

import gradio as gr
from PIL import Image, ImageDraw, ImageFont

from util.fonts_opt import is_fonts

ROOT_PATH = sys.path[0]  # 根目录
DESCRIPTION = '''# Watermarking Lab Batch'''


def Hex_to_RGB(hex):
    r = int(hex[1:3], 16)
    g = int(hex[3:5], 16)
    b = int(hex[5:7], 16)

    return r, g, b


# 目录操作
def dir_opt(target_dir):
    if os.path.exists(target_dir):
        shutil.rmtree(target_dir)
        os.mkdir(target_dir)
    else:
        os.mkdir(target_dir)


# 文件压缩
def zipDir(origin_dir, compress_file):
    # --------------- 压缩 ---------------
    zip = zipfile.ZipFile(f"{compress_file}", "w", zipfile.ZIP_DEFLATED)
    for path, dirnames, filenames in os.walk(f"{origin_dir}"):
        fpath = path.replace(f"{origin_dir}", '')
        for filename in filenames:
            zip.write(os.path.join(path, filename), os.path.join(fpath, filename))
    zip.close()


def watermarking(imgs, text, text_size, text_font, wm_location, wm_trans, wm_textColor):

    text_size = int(text_size)

    wm_imgs = []
    imgwm_path_list = []
    img_save_dir = "./wm_batch"
    dir_opt(img_save_dir)

    for img in imgs:
        img_ = Image.open(img.name)
        img_base = img_.convert("RGBA")
        txt = Image.new('RGBA', img_base.size, (255, 255, 255, 0))
        draw = ImageDraw.Draw(txt)

        font = ImageFont.truetype(f"./fonts/{text_font}.ttf", text_size)
        textwidth, textheight = draw.textsize(text, font)

        width, height = img_.size  # 图片尺寸

        # ---------- 水印位置 ----------
        if wm_location == "center":
            x = width / 2 - textwidth / 2
            y = height / 2 - textheight / 2
        elif wm_location == "bottom right":
            x = width - textwidth - text_size
            y = height - textheight - text_size
        elif wm_location == "bottom left":
            x = text_size
            y = height - textheight - text_size
        elif wm_location == "top right":
            x = width - textwidth - text_size
            y = text_size
        elif wm_location == "top left":
            x = text_size
            y = text_size

        r, g, b = Hex_to_RGB(wm_textColor)
        draw.text((x, y), text, fill=(r, g, b, math.ceil(float(wm_trans) * 255)), font=font)
        img_combined = Image.alpha_composite(img_base, txt)
        wm_imgs.append(img_combined)

        img_name_ = (img.name[:-12] + img.name[-4:]).split("/")[2]
        img_.save(f"{img_save_dir}/{img_name_}")
        imgwm_path_list.append(f"{img_save_dir}/{img_name_}")

    zipDir(img_save_dir, "wm_batch.zip")  # 下载打包文件

    return wm_imgs, imgwm_path_list, "wm_batch.zip"


def main():
    is_fonts(f"{ROOT_PATH}/fonts")  # 检查字体文件

    with gr.Blocks(css='style.css') as wml:
        gr.Markdown(DESCRIPTION)
        with gr.Row():
            with gr.Column():
                with gr.Row():
                    input_imgs = gr.File(file_count="multiple", type="file", label="原始图片")
                with gr.Row():
                    wm_location = gr.Radio(choices=["center", "bottom right", "bottom left", "top right", "top left"],
                                           value="center",
                                           label="位置")
                with gr.Row():
                    wm_text = gr.Textbox(value="水印内容", label="水印内容")
                with gr.Row():
                    wm_textFont = gr.Dropdown(choices=["SimSun", "TimesNewRoman", "malgun"], value="SimSun", label="字体")
                with gr.Row():
                    wm_textSize = gr.Number(value=50, label="文字大小")
                with gr.Row():
                    wm_textTrans = gr.Slider(minimum=0, maximum=1, value=1, step=0.1, label="水印透明度")
                with gr.Row():
                    wm_textColor = gr.ColorPicker(label="水印颜色")

                with gr.Row():
                    btn_01 = gr.Button(value='加水印', variant="primary")

            with gr.Column():
                with gr.Row():
                    output_imgs = gr.Gallery(label="水印图片")
                with gr.Row():
                    output_files = gr.File(file_count="multiple", type="file", label="水印图片下载")
                with gr.Row():
                    output_files_batch = gr.File(type="file", label="水印图片批量下载")

        with gr.Row():
            sheets = "img_examples"
            example_list = [[[os.path.join(sheets, x) for x in os.listdir(sheets)], "Watermarking Text", 50,
                             "TimesNewRoman", "bottom right", "0.9", "#FFFFFF"]]
            gr.Examples(example_list,
                        [input_imgs, wm_text, wm_textSize, wm_textFont, wm_location, wm_textTrans, wm_textColor],
                        [output_imgs, output_files, output_files_batch],
                        watermarking,
                        cache_examples=False)

        btn_01.click(fn=watermarking,
                     inputs=[input_imgs, wm_text, wm_textSize, wm_textFont, wm_location, wm_textTrans, wm_textColor],
                     outputs=[output_imgs, output_files, output_files_batch])

    wml.launch(inbrowser=True)


if __name__ == '__main__':
    main()
