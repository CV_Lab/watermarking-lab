# Watermarking Lab
# 创建人：曾逸夫
# 创建时间：2022-08-10
# 功能描述：单一图片加水印 透明度 字体颜色

import math
import sys

import gradio as gr
from PIL import Image, ImageDraw, ImageFont

from util.fonts_opt import is_fonts

ROOT_PATH = sys.path[0]  # 根目录
DESCRIPTION = '''# Watermarking Lab'''


def Hex_to_RGB(hex):
    r = int(hex[1:3], 16)
    g = int(hex[3:5], 16)
    b = int(hex[5:7], 16)

    return r, g, b


def watermarking(img, text, text_size, text_font, wm_location, wm_trans, wm_textColor):
    # 参考：https://pillow.readthedocs.io/en/stable/reference/ImageDraw.html?highlight=text#example-draw-partial-opacity-text

    img_base = img.convert("RGBA")
    txt = Image.new('RGBA', img_base.size, (255, 255, 255, 0))

    text_size = int(text_size)
    draw = ImageDraw.Draw(txt)

    font = ImageFont.truetype(f"./fonts/{text_font}.ttf", text_size)
    textwidth, textheight = draw.textsize(text, font)

    width, height = img_base.size  # 图片尺寸

    # ---------- 水印位置 ----------
    if wm_location == "center":
        x = width / 2 - textwidth / 2
        y = height / 2 - textheight / 2
    elif wm_location == "bottom right":
        x = width - textwidth - text_size
        y = height - textheight - text_size
    elif wm_location == "bottom left":
        x = text_size
        y = height - textheight - text_size
    elif wm_location == "top right":
        x = width - textwidth - text_size
        y = text_size
    elif wm_location == "top left":
        x = text_size
        y = text_size

    r, g, b = Hex_to_RGB(wm_textColor)
    draw.text((x, y), text, fill=(r, g, b, math.ceil(float(wm_trans) * 255)), font=font)
    img_combined = Image.alpha_composite(img_base, txt)

    return img_combined


def main():
    is_fonts(f"{ROOT_PATH}/fonts")  # 检查字体文件

    with gr.Blocks(css='style.css') as wml:
        gr.Markdown(DESCRIPTION)
        with gr.Row():
            with gr.Column():
                with gr.Row():
                    input_img = gr.Image(image_mode="RGB", source="upload", type="pil", label="原始图片")
                with gr.Row():
                    wm_location = gr.Radio(choices=["center", "bottom right", "bottom left", "top right", "top left"],
                                           value="center",
                                           label="位置")
                with gr.Row():
                    wm_text = gr.Textbox(value="水印内容", label="水印内容")
                with gr.Row():
                    wm_textFont = gr.Dropdown(choices=["SimSun", "TimesNewRoman", "malgun"], value="SimSun", label="字体")
                with gr.Row():
                    wm_textSize = gr.Number(value=50, label="文字大小")
                with gr.Row():
                    wm_textTrans = gr.Slider(minimum=0, maximum=1, value=1, step=0.1, label="水印透明度")
                with gr.Row():
                    wm_textColor = gr.ColorPicker(label="水印颜色")
                with gr.Row():
                    btn_01 = gr.Button(value='加水印', variant="primary")

            with gr.Column():
                with gr.Row():
                    output_img = gr.Image(type="pil", label="水印图片")

        with gr.Row():
            example_list = [[
                "./img_examples/bus.jpg", "Watermarking Text", 50, "TimesNewRoman", "bottom right", "0.9", "#FFFFFF"],
                            ["./img_examples/zidane.jpg", "水印文字", 50, "SimSun", "center", "0.5", "#A4F9AA"]]
            gr.Examples(example_list,
                        [input_img, wm_text, wm_textSize, wm_textFont, wm_location, wm_textTrans, wm_textColor],
                        output_img,
                        watermarking,
                        cache_examples=False)

        btn_01.click(fn=watermarking,
                     inputs=[input_img, wm_text, wm_textSize, wm_textFont, wm_location, wm_textTrans, wm_textColor],
                     outputs=[output_img])

    wml.launch(inbrowser=True)


if __name__ == '__main__':
    main()
