<p align="center">
<a href="https://gitee.com/CV_Lab/gradio_yolov5_det">
<img src="https://pycver.gitee.io/ows-pics/imgs/watermarking_lab_logo.png" alt="Simple Icons" >
</a>
<p align="center">
    基于Gradio的加水印工具，包括（批量）图片水印和视频水印
</p>
</p>
<p align="center">
<a href="./CodeCheck.md"><img src="https://img.shields.io/badge/CodeCheck-passing-success" alt="code check" /></a>
<a href="https://gitee.com/CV_Lab/gradio_yolov5_det/releases/v0.1"><img src="https://img.shields.io/badge/Releases-v0.1-green" alt="Releases Version" /></a>
<a href="https://huggingface.co/"><img src="https://img.shields.io/badge/%F0%9F%A4%97-Hugging%20Face-blue" alt="Hugging Face Spaces" /></a>
<a href="https://huggingface.co/spaces"><img src="https://img.shields.io/badge/🤗%20Hugging%20Face-Spaces-blue" alt="Hugging Face Spaces" /></a>
<a href="https://gitee.com/CV_Lab/gradio_yolov5_det/blob/master/LICENSE"><img src="https://img.shields.io/badge/License-GPL--3.0-blue" alt="License" /></a>
</p>
<p align="center">
<a href="https://github.com/gradio-app/gradio"><img src="https://img.shields.io/badge/Gradio-3.1.4+-orange" alt="Gradio Version" /></a>
<a href="#"><img src="https://img.shields.io/badge/Python-3.8%2B-blue?logo=python" alt="Python Version" /></a>
<a href="https://github.com/pre-commit/pre-commit"><img src="https://img.shields.io/badge/checks-pre--commit-brightgreen" alt="pre-commit"></a>
</p>



## 🚀 作者简介

曾逸夫，从事人工智能研究与开发；主研领域：计算机视觉；[YOLOv5官方开源项目代码贡献人](https://github.com/ultralytics/yolov5/graphs/contributors)；[YOLOv5 v6.1代码贡献人](https://github.com/ultralytics/yolov5/releases/tag/v6.1)；[Gradio官方开源项目代码贡献人](https://github.com/gradio-app/gradio/graphs/contributors)

❤️  Github：https://github.com/Zengyf-CVer

🔥 YOLOv5 官方开源项目PR ID：

- Save \*.npy features on detect.py `--visualize`：https://github.com/ultralytics/yolov5/pull/5701
- Fix `detect.py --view-img` for non-ASCII paths：https://github.com/ultralytics/yolov5/pull/7093
- Fix Flask REST API：https://github.com/ultralytics/yolov5/pull/7210
- Add yesqa to precommit checks：https://github.com/ultralytics/yolov5/pull/7511
- Add mdformat to precommit checks and update other version：https://github.com/ultralytics/yolov5/pull/7529
- Add TensorRT dependencies：https://github.com/ultralytics/yolov5/pull/8553

💡 YOLOv5 v6.1代码贡献链接：

- https://github.com/ultralytics/yolov5/releases/tag/v6.1

🔥 Gradio 官方开源项目PR ID：

- Create a color generator demo：https://github.com/gradio-app/gradio/pull/1872

<h2 align="center">🚀更新走势</h2>

- `2022-08-11` **⚡ [Watermarking Lab v0.1](https://gitee.com/CV_Lab/watermarking-lab/releases/v0.1)正式上线**

<h2 align="center">🤗在线Demo</h2>

### ❤️ 快速体验

本项目提供了**在线demo**，点击下面的logo，进入**Hugging Face Spaces**中快速体验：

<div align="center" >
<a href="https://huggingface.co/spaces/Zengyf-CVer/watermarking_lab">
<img src="https://pycver.gitee.io/ows-pics/imgs/huggingface_logo.png">
</a>
</div>



<h2 align="center">💎项目流程与用途</h2>



### 📌 项目整体流程

<div align="center" >
<img src="https://pycver.gitee.io/ows-pics/imgs/watermarking_lab_workflow.png">
</div>




### 📌 项目示例

#### ❤️ 输入界面（单一图片水印）

<div align="center" >
<img src="https://pycver.gitee.io/ows-pics/imgs/wm_img01.png">
</div>

<div align="center" >
输入界面01
</div>

#### ❤️ 输出界面（单一图片水印）

<div align="center" >
<img src="https://pycver.gitee.io/ows-pics/imgs/wm_img02.png">
</div>

<div align="center" >
输出界面01
</div>

#### ❤️ 示例界面（单一图片水印）

<div align="center" >
<img src="https://pycver.gitee.io/ows-pics/imgs/wm_img03.png">
</div>

<div align="center" >
示例界面01
</div>


#### ❤️ 输入界面（批量图片水印）

<div align="center" >
<img src="https://pycver.gitee.io/ows-pics/imgs/wm_batch_imgs01.png">
</div>

<div align="center" >
输入界面01
</div>

#### ❤️ 输出界面（批量图片水印）

<div align="center" >
<img src="https://pycver.gitee.io/ows-pics/imgs/wm_batch_imgs02.png">
</div>

<div align="center" >
输出界面01
</div>

#### ❤️ 示例界面（批量图片水印）

<div align="center" >
<img src="https://pycver.gitee.io/ows-pics/imgs/wm_batch_imgs03.png">
</div>

<div align="center" >
示例界面01
</div>



#### ❤️ 输入界面（视频水印）

<div align="center" >
<img src="https://pycver.gitee.io/ows-pics/imgs/wm_video02.png">
</div>

<div align="center" >
输入界面01
</div>

#### ❤️ 输出界面（视频水印）

<div align="center" >
<img src="https://pycver.gitee.io/ows-pics/imgs/wm_video01.png">
</div>

<div align="center" >
输出界面01
</div>

#### ❤️ 示例界面（视频水印）

<div align="center" >
<img src="https://pycver.gitee.io/ows-pics/imgs/wm_video03.png">
</div>

<div align="center" >
示例界面01
</div>




<h2 align="center">💡项目结构</h2>

```shell
.
├── watermarking-lab						# 项目名称
│   ├── util								# 工具包
│   │   └── fonts_opt.py					# 字体管理
│   ├── img_examples						# 示例图片
│   ├── video_examples						# 示例视频
│   ├── __init__.py							# 初始化文件
│   ├── wm_img.py							# 图片水印
│   ├── wm_img_02.py						# 图片水印02
│   ├── wm_batch_imgs.py					# 批量图片水印
│   ├── wm_video.py							# 视频水印
│   ├── LICENSE								# 项目许可
│   ├── CodeCheck.md						# 代码检查
│   ├── .gitignore							# git忽略文件
│   ├── README.md							# 项目说明
│   └── requirements.txt					# 脚本依赖包
```

<h2 align="center">🔥安装教程</h2>

### ✅ 第一步：创建conda环境

```shell
conda create -n wm python==3.8
conda activate wm # 进入环境
```

### ✅ 第二步：克隆

```shell
git clone https://gitee.com/CV_Lab/watermarking-lab.git
```

### ✅ 第三步：安装Watermarking Lab依赖

```shell
cd watermarking-lab
pip install -r ./requirements.txt -U
```



<h2 align="center">⚡使用教程</h2>

### 💡 单一图片水印

```shell
python wm_img_02.py  # 推荐
python wm_img.py
```

### 💡 批量图片水印

```shell
python wm_batch_img.py
```

### 💡 视频水印

```shell
python wm_video.py
```



### 💬 技术交流

- 如果你发现任何Watermarking Lab存在的问题或者是建议, 欢迎通过[Gitee Issues](https://gitee.com/CV_Lab/watermarking-lab/issues)给我提issues。
- 欢迎加入CV Lab技术交流群

<div align="center" >
<img src="https://pycver.gitee.io/ows-pics/imgs/qq_group.jpg" width="20%">
</div>
