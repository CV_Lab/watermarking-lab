# Watermarking Lab
# 创建人：曾逸夫
# 创建时间：2022-08-09
# 功能描述：视频水印

import gc
import math
import os
import shutil
import sys

import cv2
import gradio as gr
import numpy as np
from PIL import Image, ImageDraw, ImageFont

from util.fonts_opt import is_fonts

ROOT_PATH = sys.path[0]  # 根目录
DESCRIPTION = '''# Watermarking Lab Video'''


# 目录操作
def dir_opt(target_dir):
    if os.path.exists(target_dir):
        shutil.rmtree(target_dir)
        os.mkdir(target_dir)
    else:
        os.mkdir(target_dir)


def Hex_to_RGB(hex):
    r = int(hex[1:3], 16)
    g = int(hex[3:5], 16)
    b = int(hex[5:7], 16)

    return r, g, b


def watermarking(video, text, text_size, text_font, wm_location, wm_trans, wm_textColor):

    if video is None or video == "":
        # 判断是否有视频存在
        print("视频不存在！")
        return None

    os.system("""
        if [ -e './output.mp4' ]; then
        rm ./output.mp4
        fi
        """)

    text_size = int(text_size)

    img_save_dir = "./wm_batch"
    dir_opt(img_save_dir)

    # video->frame
    gc.collect()
    output_video_path = "./output.avi"
    cap = cv2.VideoCapture(video)
    fourcc = cv2.VideoWriter_fourcc(*"I420")  # 编码器

    out = cv2.VideoWriter(output_video_path, fourcc, 30.0, (int(cap.get(3)), int(cap.get(4))))
    if cap.isOpened():
        while cap.isOpened():
            ret, frame = cap.read()
            # 判断空帧
            if not ret:
                break

            frame = Image.fromarray(cv2.cvtColor(frame, cv2.COLOR_BGR2RGB))
            img_base = frame.convert("RGBA")
            txt = Image.new('RGBA', img_base.size, (255, 255, 255, 0))
            draw = ImageDraw.Draw(txt)

            font = ImageFont.truetype(f"./fonts/{text_font}.ttf", text_size)
            textwidth, textheight = draw.textsize(text, font)

            width, height = frame.size  # 图片尺寸

            # ---------- 水印位置 ----------
            if wm_location == "center":
                x = width / 2 - textwidth / 2
                y = height / 2 - textheight / 2
            elif wm_location == "bottom right":
                x = width - textwidth - text_size
                y = height - textheight - text_size
            elif wm_location == "bottom left":
                x = text_size
                y = height - textheight - text_size
            elif wm_location == "top right":
                x = width - textwidth - text_size
                y = text_size
            elif wm_location == "top left":
                x = text_size
                y = text_size

            r, g, b = Hex_to_RGB(wm_textColor)
            draw.text((x, y), text, fill=(r, g, b, math.ceil(float(wm_trans) * 255)), font=font)
            img_combined = Image.alpha_composite(img_base, txt)

            frame = cv2.cvtColor(np.asarray(img_combined), cv2.COLOR_RGB2BGR)

            # frame->video
            out.write(frame)

        out.release()
        cap.release()
        return output_video_path
    else:
        print("视频加载失败！")
        return None


def main():
    is_fonts(f"{ROOT_PATH}/fonts")  # 检查字体文件

    with gr.Blocks(css='style.css') as wml:
        gr.Markdown(DESCRIPTION)
        with gr.Row():
            with gr.Column():
                with gr.Row():
                    input_video = gr.Video(format="mp4", source="upload", mirror_webcam=False, label="原始视频")  # 上传
                with gr.Row():
                    wm_location = gr.Radio(choices=["center", "bottom right", "bottom left", "top right", "top left"],
                                           value="center",
                                           label="位置")
                with gr.Row():
                    wm_text = gr.Textbox(value="水印内容", label="水印内容")
                with gr.Row():
                    wm_textFont = gr.Dropdown(choices=["SimSun", "TimesNewRoman", "malgun"], value="SimSun", label="字体")
                with gr.Row():
                    wm_textSize = gr.Number(value=50, label="文字大小")
                with gr.Row():
                    wm_textTrans = gr.Slider(minimum=0, maximum=1, value=1, step=0.1, label="水印透明度")
                with gr.Row():
                    wm_textColor = gr.ColorPicker(label="水印颜色")

                with gr.Row():
                    btn_01 = gr.Button(value='加水印', variant="primary")

            with gr.Column():
                with gr.Row():
                    output_video = gr.Video(format='mp4', label="检测视频")

        with gr.Row():
            example_list = [[
                "./video_examples/test01.mp4", "Watermarking Text", 50, "TimesNewRoman", "bottom right", "0.5",
                "#A4F9AA"]]
            gr.Examples(example_list,
                        [input_video, wm_text, wm_textSize, wm_textFont, wm_location, wm_textTrans, wm_textColor],
                        output_video,
                        watermarking,
                        cache_examples=False)

        btn_01.click(fn=watermarking,
                     inputs=[input_video, wm_text, wm_textSize, wm_textFont, wm_location, wm_textTrans, wm_textColor],
                     outputs=[output_video])

    wml.launch(inbrowser=True)


if __name__ == '__main__':
    main()
